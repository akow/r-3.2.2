Package: adegraphics
Type: Package
Title: An S4 Lattice-Based Package for the Representation of
        Multivariate Data
Version: 1.0-3
Date: 2015-06-23
Author: Stéphane Dray <stephane.dray@univ-lyon1.fr> and Aurélie Siberchicot <aurelie.siberchicot@univ-lyon1.fr>, with contributions from Jean Thioulouse. Based on earlier work by Alice Julien-Laferrière.
Maintainer: Aurélie Siberchicot <aurelie.siberchicot@univ-lyon1.fr>
Description: Graphical functionalities for the representation of multivariate data. It is a complete re-implementation of the functions available in the 'ade4' package.
Depends: R (>= 3.0.2)
License: GPL (>= 2)
Imports: ade4, grid, KernSmooth, lattice, methods, RColorBrewer, sp (>=
        1.1-1)
Suggests: Guerry, maptools, pixmap, spdep, splancs
Collate: adeGsenv.R parameter.R utils.R utilstriangle.R genericMethod.R
        utilsclass.R panelfunctions.R ADEg.R ADEgS.R utilsADEgS.R
        ADEg.C1.R C1.barchart.R C1.curve.R C1.density.R C1.gauss.R
        C1.dotplot.R C1.hist.R C1.interval.R ADEg.S1.R S1.boxplot.R
        S1.class.R S1.distri.R S1.label.R S1.match.R ADEg.S2.R
        S2.arrow.R S2.class.R S2.corcircle.R S2.density.R S2.distri.R
        S2.image.R S2.label.R S2.logo.R S2.match.R S2.traject.R
        S2.value.R ADEg.T.R T.image.R T.value.R T.cont.R ADEg.Tr.R
        Tr.class.R Tr.label.R Tr.match.R Tr.traject.R ade4-kplot.R
        ade4-scatter.R ade4-plot.R s.hist.R multiplot.R s.Spatial.R
        utilskey.R
URL: http://pbil.univ-lyon1.fr/ADE-4, Mailing list:
        http://listes.univ-lyon1.fr/wws/info/adelist
Encoding: UTF-8
Repository: CRAN
Repository/R-Forge/Project: ade4
Repository/R-Forge/Revision: 816
Repository/R-Forge/DateTimeStamp: 2015-06-23 11:27:37
Date/Publication: 2015-06-24 17:28:27
NeedsCompilation: no
Packaged: 2015-06-23 14:06:13 UTC; rforge
Built: R 3.2.2; ; 2015-11-03 01:14:42 UTC; unix
